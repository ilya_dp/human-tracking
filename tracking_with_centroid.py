import os.path
import cv2
import click
from entities.centroidtracker import CentroidTracker
from entities.trackableobject import TrackableObject
from entities.utils import video_detections
from tqdm import tqdm


@click.command()
@click.option('--detections_filename')
@click.option('--video_filename')
@click.option('--centroid_max_dissapeared', default=100)
@click.option('--centroid_max_distance', default=100)
@click.option('--human_detection_thresh', default=0)
@click.option('--min_centroids_to_track', default=5)
@click.option('--min_movement', default=0.01)
@click.option('--show_frame', default=False)
@click.option('--show_timer_after', default=5)
@click.option('--output_video')
def track(detections_filename, video_filename, centroid_max_dissapeared, centroid_max_distance,
          human_detection_thresh, min_centroids_to_track, min_movement, show_frame, output_video,
          show_timer_after):

    assert os.path.isfile(video_filename), f"File {video_filename} not found"

    ct = CentroidTracker(maxDisappeared=centroid_max_dissapeared, maxDistance=centroid_max_distance)
    trackableObjects = {}
    # font = cv2.FONT_HERSHEY_SIMPLEX
    font = cv2.FONT_ITALIC

    cap = cv2.VideoCapture(video_filename)
    fps = int(cap.get(cv2.CAP_PROP_FPS))
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    print(f"Frame width {width}")
    print(f"Frame height {height}")
    print(f"Camera FPS is {fps}")

    writer = None

    for frame_id, detections in tqdm(video_detections(detections_filename, width=width, height=height)):
        ret, img = cap.read()
        cv2.putText(img, str(frame_id), (width - 60, 20), font, 0.5, (0, 255, 0), 2)
        rects = [d[:4] for d in detections if d[-1] > human_detection_thresh]
        objects = ct.update(rects)

        # loop over the tracked objects
        for (objectID, centroid) in objects.items():
            to = trackableObjects.get(objectID, None)
            if to is None:
                to = TrackableObject(
                    centroid, n_centroids_to_track=min_centroids_to_track,
                    movement=min_movement, fps=fps
                )
            else:
                to.update(centroid)

            trackableObjects[objectID] = to

            if to.is_tracked:
                (startX, startY, endX, endY) = centroid
                cX = int((startX + endX) / 2.0)
                cY = int((startY + endY) / 2.0)
                cv2.putText(img, str(to), (cX - 10, cY - 10), font, 0.5, (0, 255, 0), 2)
                cv2.circle(img, (cX, cY), 4, (255, 0, 0), -1)
                if to.time >= show_timer_after:
                    cv2.putText(img, str(to.time) + " sec", (cX - 10, cY + 20), font, 0.5, (255, 0, 0), 2)

        if output_video is not None and writer is None:
            fourcc = cv2.VideoWriter_fourcc(*"mp4v")
            writer = cv2.VideoWriter(output_video, fourcc, fps, (width, height), True)

        if writer is not None:
            writer.write(img)

        if show_frame:
            cv2.imshow('Video', img)
            if cv2.waitKey(30) & 0xFF == ord('q'):
                break

    cap.release()
    cv2.destroyAllWindows()
    if writer is not None:
        writer.release()


if __name__ == '__main__':
    track()



