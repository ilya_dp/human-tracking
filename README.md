# Требования 
python==3.8.5

# Установка 
```
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt

# установить репозиторий по обнаружению людей
git clone https://github.com/tensorboy/pytorch_Realtime_Multi-Person_Pose_Estimation.git
cd pytorch_Realtime_Multi-Person_Pose_Estimation/
git submodule init && git submodule update
cd lib/pafprocess
sh make.sh
```
### Cкачать веса модели (по [ссылке](https://drive.google.com/file/d/1nAwKxKynKHxP8eRFZeR-d37gNu4o3aee/view?usp=sharing) )
в папку human-tracking/weights


# Использование
### Шаг 1. Найти людей на видео
``
python human_detection.py \
    --detector_weights=weights/pose_model.pth \ 
    --video_filename=1.mp4 \
    --result_detections_filename=2.csv
``

### Шаг 2. Запустить трекер
``
python tracking_with_centroid.py \
    --detections_filename=1.csv \
    --video_filename=1.mp4 \
    --centroid_max_dissapeared=100 \
    --centroid_max_distance=100 \
    --human_detection_thresh=0 \
    --min_centroids_to_track=5 \
    --min_movement=0.01 \
    --show_frame=True \
    --output_video=1_tracking.mp4
``