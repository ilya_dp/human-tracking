import sys
import os
import torch
from collections import namedtuple
import numpy as np
base_path = os.path.join(
    os.path.dirname(os.path.dirname(__file__)),
    'pytorch_Realtime_Multi-Person_Pose_Estimation'
)
sys.path.append(base_path)
from lib.network.rtpose_vgg import get_model
from lib.config import update_config, cfg
from lib.utils.paf_to_pose import paf_to_pose_cpp
from lib.utils.common import draw_humans
from lib.datasets.preprocessing import rtpose_preprocess
from lib.network import im_transform


configs = namedtuple('configs', ['cfg', 'weight', 'opts'])
if torch.cuda.is_available():
    torch.backends.cudnn.enabled = True
    torch.backends.cudnn.benchmark = True


class HumanDetector:
    def __init__(self, model_path):
        self.model_path = model_path
        args = configs(
            cfg=os.path.join(base_path, 'experiments/vgg19_368x368_sgd.yaml'),
            weight=self.model_path,
            opts=[]
        )
        update_config(cfg, args)
        self.model = get_model('vgg19')
        self.model.load_state_dict(torch.load(args.weight))
        if torch.cuda.is_available():
            self.model.cuda()
        self.model.float()
        self.model.eval()

    def get_outputs(self, img):
        inp_size = cfg.DATASET.IMAGE_SIZE
        im_croped, im_scale, real_shape = im_transform.crop_with_factor(
            img, inp_size, factor=cfg.MODEL.DOWNSAMPLE, is_ceil=True)

        im_data = rtpose_preprocess(im_croped)
        batch_images = np.expand_dims(im_data, 0)

        # several scales as a batch
        batch_var = torch.from_numpy(batch_images)
        if torch.cuda.is_available():
            batch_var = batch_var.cuda()
        batch_var = batch_var.float()
        predicted_outputs, _ = self.model(batch_var)
        output1, output2 = predicted_outputs[-2], predicted_outputs[-1]
        heatmap = output2.cpu().data.numpy().transpose(0, 2, 3, 1)[0]
        paf = output1.cpu().data.numpy().transpose(0, 2, 3, 1)[0]

        return paf, heatmap, im_scale

    def detect(self, img, draw=False):
        with torch.no_grad():
            paf, heatmap, imscale = self.get_outputs(img)
            humans = paf_to_pose_cpp(heatmap, paf, cfg)
        if draw:
            draw_humans(img, humans)
        return humans
