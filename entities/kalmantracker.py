import numpy as np
from .kalmanfilter import KalmanFilter
from scipy.optimize import linear_sum_assignment
from collections import deque
import random


class Tracks:
    def __init__(self, detection, trackId):
        self.KF = KalmanFilter(dt=1.5, stateVariance=1.5, measurementVariance=1.5, method='Velocity')
        self.KF.predict()
        self.KF.correct(np.matrix(detection).reshape(2, 1))
        self.prediction = detection.reshape(1, 2)
        self.trace = deque(self.prediction, maxlen=50)
        self.trackId = trackId
        self.skipped_frames = 0
        self.color = (
            random.randint(0, 255),
            random.randint(0, 255),
            random.randint(0, 255)
        )
        self.is_verified = False

    @property
    def distance(self):
        if len(self.trace) > 2:
            first = self.trace[0]
            last = np.array(self.trace[-1])
            dist = np.linalg.norm(first - last.reshape(-1, 2), axis=1)
            return int(dist[0])
        return 0

    def verify(self):
        if not self.is_verified:
            self.is_verified = self.distance > 0

    @property
    def disappeared(self):
        return self.skipped_frames > 10

    def predict(self, detection):
        self.prediction = np.array(self.KF.predict()).reshape(1, 2)
        self.KF.correct(np.matrix(detection).reshape(2, 1))


class Tracker:

    def __init__(self, dist_threshold, max_frame_skipped):
        self.dist_threshold = dist_threshold
        self.max_frame_skipped = max_frame_skipped
        self.trackId = 0
        self.tracks = []

    def update(self, detections):

        if len(self.tracks) == 0 and len(detections) > 0:
            for i in range(detections.shape[0]):
                track = Tracks(detections[i], self.trackId)
                self.trackId += 1
                self.tracks.append(track)

        N = len(self.tracks)
        assignment = [-1] * N

        if len(detections) > 0:
            cost = []
            for i in range(N):
                cur_track = self.tracks[i]
                diff = np.linalg.norm(cur_track.trace[-1] - detections.reshape(-1, 2), axis=1)
                # diff = np.linalg.norm(self.tracks[i].prediction - detections.reshape(-1, 2), axis=1)
                cost.append(diff)

            cost = np.array(cost)
            row, col = linear_sum_assignment(cost)

            for i in range(len(row)):
                assignment[row[i]] = col[i]

            for i in range(len(assignment)):
                if cost[i][assignment[i]] > self.dist_threshold:
                    assignment[i] = -1

        for i in range(len(assignment)):
            if assignment[i] == -1:
                self.tracks[i].skipped_frames += 1

        del_tracks = []
        for i in range(len(self.tracks)):
            if self.tracks[i].skipped_frames > self.max_frame_skipped:
                del_tracks.append(i)

        if len(del_tracks) > 0:
            for i in range(len(del_tracks)):
                del self.tracks[i]
                del assignment[i]

        for i in range(len(detections)):
            if i not in assignment:
                track = Tracks(detections[i], self.trackId)
                self.trackId += 1
                if self.trackId == 3:
                    print(1)
                self.tracks.append(track)

        for i in range(len(assignment)):
            if assignment[i] != -1:
                detection = detections[assignment[i]]
                self.tracks[i].skipped_frames = 0
                self.tracks[i].predict(detection)
                self.tracks[i].trace.append(detection)
            # else:
            #     self.tracks[i].trace.append(self.tracks[i].prediction[0])
