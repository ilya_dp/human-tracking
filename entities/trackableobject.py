import numpy as np


class TrackingCounter:
    def __init__(self):
        self.n = 1

    def get(self):
        n = self.n
        self.n += 1
        return n


Counter = TrackingCounter()


class TrackableObject:
    def __init__(self, centroid, n_centroids_to_track=5, movement=0.05, fps=16):
        self.centroids = [centroid]
        self.n_centroids_to_track = n_centroids_to_track
        self.movement = movement
        self.fps = fps
        self.tracking_id = None
        self.is_tracked = False
        self.frames_count = 1

    @property
    def time(self):
        return self.frames_count // self.fps

    def _track(self):
        if not self.is_tracked:
            self.tracking_id = Counter.get()
            self.is_tracked = True

    def update(self, centroid):
        self.centroids.append(centroid)
        self.frames_count += 1
        x = np.array([c[0] for c in self.centroids])
        y = np.array([c[1] for c in self.centroids])

        if (len(self.centroids) > self.n_centroids_to_track) and \
            (x.std() > self.movement or y.std() > self.movement):
            self._track()

    def __str__(self):
        if not self.tracking_id:
            return ''
        return f"id-{self.tracking_id}"

