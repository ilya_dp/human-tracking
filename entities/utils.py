import numpy as np
import pandas as pd


def humans_to_bboxes(humans):
    bboxes, scores = [], []
    for human in humans:
        xS = [p.x for p in human.body_parts.values()]
        yS = [p.y for p in human.body_parts.values()]
        startX, startY = min(xS), min(yS)
        endX, endY = max(xS), max(yS)
        bboxes.append((startX, startY, endX, endY))
        scores.append(human.score)
    return bboxes, scores


def to_tracker_row(row, width, height):
    return np.array([
        [
            int(r['x_min'] * width),
            int(r['y_min'] * height),
            int(r['x_max'] * width),
            int(r['y_max'] * height),
            int(r['human_score'])
        ]
        for r in row if not np.isnan(r['x_min'])]
    )


def video_detections(filename, width, height):
    df = pd.read_csv(filename)
    for _, group in df.groupby('frame_id'):
        row = group.to_dict('records')
        frame_id = row[0]['frame_id']
        yield frame_id, to_tracker_row(row, width, height)


def centroid_detections(filename, width, height, min_area=0., max_area=1.):
    total_area = width * height
    for _, frame in video_detections(filename, width, height):
        res = []
        for detection in frame:
            (startX, startY, endX, endY, score) = detection
            rel_area = (endX - startX) * (endY - startY) / total_area
            cX = int((startX + endX) / 2.0)
            cY = int((startY + endY) / 2.0)
            if min_area <= rel_area <= max_area:
                res.append((cX, cY))
        yield np.array(res)


def rect_area(rect):
    x1, y1, x2, y2 = rect
    return (x2 - x1) * (y2 - y1)


def filter_rects_by_area(rects, min_area=100, max_area=1000):
    return [r for r in rects if min_area > rect_area(r) > max_area]