import cv2
import csv
import click
from entities.humandetector import HumanDetector
from entities.utils import humans_to_bboxes
from tqdm import tqdm


def get_default_row(frame_id):
    return {
        'frame_id': frame_id,
        'human_id': None,
        'human_score': None,
        'x_min': None,
        'y_min': None,
        'x_max': None,
        'y_max': None
    }


@click.command()
@click.option('--detector_weights', default='weights/pose_model.pth')
@click.option('--video_filename', default='1.mp4')
@click.option('--result_detections_filename', default='1.csv')
def main(detector_weights, video_filename, result_detections_filename):
    detector = HumanDetector(model_path=detector_weights)
    cap = cv2.VideoCapture(video_filename)
    frames_count = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    field_names = ['frame_id', 'human_id', 'human_score', 'x_min', 'y_min', 'x_max', 'y_max']
    with open(result_detections_filename, 'w') as f:
        writer = csv.DictWriter(f, fieldnames=field_names)
        writer.writeheader()
        for frame_id in tqdm(range(frames_count)):
            ret, img = cap.read()
            if not ret:
                continue
            humans = detector.detect(img, draw=True)
            if len(humans) == 0:
                row = get_default_row(frame_id)
                writer.writerow(row)
            else:
                bboxes, scores = humans_to_bboxes(humans)
                for human_id, (bbox, score) in enumerate(zip(bboxes, scores)):
                    row = {
                        'frame_id': frame_id,
                        'human_id': human_id,
                        'human_score': score,
                        'x_min': bbox[0],
                        'y_min': bbox[1],
                        'x_max': bbox[2],
                        'y_max': bbox[3]
                    }
                    writer.writerow(row)

            cv2.imshow('Video', img)
            if cv2.waitKey(30) & 0xFF == ord('q'):
                break

        cap.release()
        cv2.destroyAllWindows()


if __name__ == '__main__':
    main()